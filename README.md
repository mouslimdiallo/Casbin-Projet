Notre projet consiste à a mise en place d’une application Web Services permettant aux médecins - responsables - d'un hôpital de contrôler, surveiller, consulter les Entrées et sorties de leurs employés pendant les heures de travail.
Nous avons les médecins auxquels on peut attribuer des rôles (attribut rôle), les heures de travail constituent les attributs d’environnement, les entrées et sorties sont des attributs d’actions, le portail d’entrée constitue l’attribut de l’objet. Ainsi nous allons pouvoir déclarer nos variables.

La réalisation de ce projet résulte d’un cas d’implémentation d’ABAC provenant de Shenlanju, le jardin des blogs. (studyzy@163.com, 2019-08-20). Voyant comment réaliser cela définition nombre de l'objet et des utilisateurs au groupe d'utilisateurs.
Nous utiliserons IntelliJ de JetBrains dans la création de nos deux fichiers : App_model.conf et App_policy.csv !

1.4	Procédure

Voici le fichier du modèle que je vais nommer : App_model.CONF et le fichier de stockage des politiques comme App_policy.csv.
[Request_definition ou définition de la demande d'accès]


r = sub, obj, act, envi



La requête correspondant à une définition de politique. Comme exemple, cette requête notée regroupe quatre éléments (r = sub, obj, act, envi) qu’on peut représenter comme suit : ["Mamadou", "Porte", "Entrer", "Heure" ] où Mamadou entre à travers une porte pendant une heure définie.

[Policy_definition ou définition de la politique]


p = sub, obj, act, eft


Pour définir le stockage de la politique, c’est dans la [policy_definition]. On peut avoir un attribut supplémentaire eft qui prend allow ou deny selon la correspondance définie dans le Matcher.

[policy_effect - autorisation ou refus des règles politiques]


e = some (where (p.eft == allow))


Ici, nous définissons si oui ou non on peut accorder une autorisation d’une demande. Allow est accepté par défaut. On peut définir Deny à la place d’Allow pour refuser l’autorisation.

[matchers ou évaluation des règles]

m = r.sub.Role == 'Docteur' && r.obj.NomPorte == 'PorteHopital' && r.act in ('Entrée', 'Sortie') && r.envi.Time.Hours >= 7 && r.envi.Time.Hours <= 20

Le Matcher est l’évaluateur en tenant compte de la correspondance de la requête r à la règle p. Noté m, nous avons défini un rôle à notre utilisateur dans l’hôpital p.sub.Role. Je veux dire par là que s'il y a une correspondance entre l'attribut du sujet qui a un rôle dans l'hôpital avec le nom de ce dernier, une correspondance avec l'objet r.obj.NomPorte qui constitue la porte de l'hôpital et l'action d'entrée à travers cette porte dans une heure définie, alors la politique autorisé.

Maintenant combinons Casbin avec notre modèle défini et notre code en langage java. Je vais définir la classe Enfoncer () et les constructeurs et spécifier le chemin des 2 fichiers.


Dans la création de mon exécuteur : utilisons JDBCAdapter de JCasbin pour charger la politique à partir d la base de données qui se trouve sur GitHub.

public Enforcer() {

    e = new Enforcer("App_model.conf", "App_policy.csv");

    e.loadPolicy();
}

ou

Enforcer e = new Enforcer("App_model.conf", "App_policy.csv");

J’ai donc créé une classe Enfoncer () où nous avons fait une instanciation pour l’exécution des règles de politiques. e.loadPolicy() permet donc de charger la politique à partir de la base de données.

Pour notre Adaptateur JCasbin, on va l’initialiser via cette méthode :
JAdapter = new JAdapter();

Dans notre code Main.java, nous avons créé plusieurs classes et déclarer nos variables comme suit :


public class Main {
    class Person
    {
        String Role ="Docteur";
        String NomUser = "Mamadou";
    }
    class Porte
    {
        String NomPorte ="PorteHopital";
    }
    class envi
    {
        String Temps;
        String Locate ="position";
    }

    public static <envi> void main(String[] args) {

            }

    private static void CompareTemps() 
{
        CompareTemps();
    
}

A la fin, nous avons appelé la fonction CompareTemps () qui est codé dans la classe Enfoncer () et qui délimite le temps d’entrée et de sortie de l’hôpital. Voyons le code.
public static <Envi> boolean CompareTemps(Envi, envi)
{

    boolean b = envi.time.getHour() >= 7 && envi.time.getHour() <= 20;
    return b;
}

Ensuite comme exemple, nous réalisons un cas d’autorisation et de sortie via une classe que j’ai appelée Fonction. 
Dans cette fonction, nous avons donc un employé du nom de Mamadou et un responsable hôpital nommé Docteur, l’opération consiste à la réalisation des actions d'entrée et de sortie dans un temps imparti. Pour cela, nous allons créer une classe TestHopitals () et faire des instanciations.


import java.util.Date;

public class Fonction {

    private static Object p1;
    private static Object e;
    private static Object modelText;

    public static <modelText> void TestHopitals() {
        p1 = new Person() {
            String Role = "Employé";
            String NomUser = "Mamadou";
        };
        p2 = new Main.Person() {
            String Role = "Docteur";
            String NomUser = "Adda";
        };

        Persons = new Person (p1, p2);

        g1  = new NomPorte() {
            String NomPorte = "Entrée";
        };
        g2  = new NomPorte() {
            String NomPorte = " Sortie ";
        };

        Portes = new Porte(g1, g2);

On devrait avoir des règles d’autorisation comme suit :
Employé mamadou Entrée PorteHopital true
Employé mamadou Sortie PorteHopital false

Voir le code source sur Gitlab : https://gitlab.com/mouslimdiallo/Casbin-Projet

Réalisé Diallo Mamadou Mouslim
